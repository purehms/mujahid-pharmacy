from staff.models import Staffs
from django.shortcuts import (
    get_object_or_404,
    HttpResponseRedirect,
)
from cart.cart import Cart
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from datetime import datetime


def cart(request):
    return {'cart': Cart(request)}


def get_today(request):
    return {'today': datetime.today()}


def get_user(user):
    return get_object_or_404(Staffs, user__username=user)


def check_request(request):
    user = get_user(request.user)
    if user.is_admin == False:
        return HttpResponseRedirect('/')


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None
