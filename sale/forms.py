from django import forms
from sale.models import ItemSale


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = ItemSale
        fields = [
            'customer',
            'phone',
        ]
