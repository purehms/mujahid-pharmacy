# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from drug.models import Drug


# Create your models here.
class ItemSale(models.Model):
    customer = models.CharField(max_length=100)
    phone = models.IntegerField(default=0)
    total_pay = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return 'ItemSale {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class Sale(models.Model):
    item = models.ForeignKey(ItemSale, related_name='items')
    product = models.ForeignKey(Drug, related_name='order_items')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity
