# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import (
    render,
    redirect,
    get_object_or_404,
    get_list_or_404,
    HttpResponseRedirect,
)
from django.urls import reverse

from cart.cart import Cart
from cart.forms import AddProductForm
from sale.forms import OrderCreateForm
from sale.models import Sale
from drug.models import Drug
from utils import get_user
from django.http import HttpResponse
from django.views.generic import View
from utils import render_to_pdf
import datetime
from django.template.loader import get_template


# Create your views here.
def sale_detail(request, key):
    product = get_object_or_404(Drug, id=key)
    form = AddProductForm()
    context = {
        'product': product,
        'form': form
    }
    return render(request, 'sale/detail.html', context)


@login_required(login_url='/?next=/')
def sale(request):
    form = OrderCreateForm(request.POST or None)
    cart = Cart(request)
    if form.is_valid():
        order = form.save()
        for item in cart:
            Sale.objects.create(
                order=order,
                product=item['product'],
                price=item['price'],
                quantity=item['quantity']
            )
        # clear the cart
        cart.cart.clear()
        cart.save()
        return HttpResponseRedirect(reverse('sale:view_sale'))
    for item in cart:
        item['update_quantity_form'] = AddProductForm(
            initial={
                'quantity': item['quantity'],
                'update': True
            }
        )
    context = {
        'products': Drug.objects.filter(visible=True),
        'user': get_user(request.user),
        'cart': cart,
        'form': form,
    }
    return render(request, 'sale/sales.html', context)


@login_required(login_url='/?next=/')
def view_sale(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    context = {
        'sales': Sale.objects.all(),
        'user': get_user(request.user),
        }
    return render(request, 'sale/view_sales.html', context)


class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        data = {
            'today': datetime.date.today(),
            'amount': 39.99,
            'customer': 'Cooper Mann',
            'order_id': 1233434,
        }
        pdf = render_to_pdf('pdf.html', data)
        return HttpResponse(pdf, content_type='application/pdf')


class GeneratePDF(View):
    def get(self, request, order, *args, **kwargs):
        template = get_template('sale/invoice-print.html')
        invoice_value = get_list_or_404(Sale, order_id=order)
        instance = invoice_value[0]
        total = 0
        for invoice_val in invoice_value:
            total += invoice_val.get_cost()
        context = {
            'today': instance.order.created,
            'invoice_values': invoice_value,
            'order_id': order,
            'total': total,
            'instance': instance,
            'invoice_id': str('AJZ' + order + 'R1'),
        }
        template.render(context)
        pdf = render_to_pdf('sale/invoice-print.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" % str('000' + order)
            content = "inline; filename='%s'" % filename
            download = self.request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % filename
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")
