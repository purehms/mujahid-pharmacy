from django.conf.urls import url
from views import (
    view_sale,
    sale_detail,
    sale,
    GeneratePDF
)

urlpatterns = [
    url(r'^$', sale,  name='sale'),
    url(r'^view/$', view_sale, name='view_sale'),
    url(r'^(?P<order>\d+).pdf/$', GeneratePDF.as_view(), name='pdf'),
    url(r'^(?P<key>\d+)/$', sale_detail, name='sale_detail'),
]
