# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import (
    render,
    redirect,
    HttpResponseRedirect,
    get_object_or_404,
)
from django.urls import reverse

from drug.forms import (
    CategoryForm,
    DrugForm,
)
from drug.models import (
    DrugCategory,
    Drug,
)
from utils import get_user


# Create your views here.


@login_required(login_url='/?next=/')
def category(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    form = CategoryForm(request.POST or None)
    if form.is_valid():
        if form.is_valid():
            name = form.cleaned_data['name']
            DrugCategory.objects.create(name=name)
            messages.success(request, 'sucessuflly add new category')
            return HttpResponseRedirect(reverse('drug:category'))
    context = {
        'categories': DrugCategory.objects.all(),
        'form': form,
        'user': get_user(request.user),
    }
    return render(request, "drug/add_category.html", context)


@login_required(login_url='/?next=/')
def add_drug(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    form = DrugForm(request.POST or None)
    if form.is_valid():
        if form.is_valid():
            batch_no = form.cleaned_data['batch_no']
            drug_name = form.cleaned_data['drug_name']
            manufacture = form.cleaned_data['manufacture']
            temperature = form.cleaned_data['temperature']
            quantity = form.cleaned_data['quantity']
            cat = form.cleaned_data['category']
            cost_price = form.cleaned_data['cost_price']
            production_date = form.cleaned_data['production_date']
            expiry_date = form.cleaned_data['expiry_date']
            total_cost = form.cleaned_data['total_cost']
            price_per_item = form.cleaned_data['price_per_item']
            drug_location = form.cleaned_data['drug_location']

            Drug.objects.create(
                batch_no=batch_no, drug_name=drug_name, manufacture=manufacture,
                category=cat, temperature=temperature, quantity=quantity,
                cost_price=cost_price, production_date=production_date,
                expiry_date=expiry_date, total_cost=total_cost,
                price_per_item=price_per_item, drug_location=drug_location
            )
            return HttpResponseRedirect(reverse('drug:view_drug'))
    context = {
        'categories': DrugCategory.objects.all(),
        'form': form,
        'user': get_user(request.user),
    }
    return render(request, "drug/add_drug.html", context)


@login_required(login_url='/?next=/')
def view_drug(request):
    product = Drug.objects.all()
    context = {
        'products': product,
        'user': get_user(request.user),
    }
    return render(request, 'drug/view_drugs.html', context)


@login_required(login_url='/?next=/')
def manage_drug(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    drug = Drug.objects.all()
    context = {
        'products': drug,
        'user': get_user(request.user),
    }
    return render(request, 'drug/manage_drug.html', context)


@login_required(login_url='/?next=/')
def delete(request, key):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    drug = get_object_or_404(Drug, id=key)
    drug.visible = False
    drug.save()
    context = {
        'products': Drug.objects.all(),
        'user': get_user(request.user),
    }
    return render(request, 'drug/manage_drug.html', context)
