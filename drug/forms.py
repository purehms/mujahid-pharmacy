from django import forms

from drug.models import DrugCategory, Drug


class CategoryForm(forms.ModelForm):
    class Meta:
        model = DrugCategory
        fields = ('name', )


class DrugForm(forms.ModelForm):
    class Meta:
        model = Drug
        fields = (
            'batch_no', 'drug_name', 'manufacture',
            'category', 'temperature', 'quantity', 'cost_price',
            'production_date', 'expiry_date', 'total_cost',
            'price_per_item', 'drug_location'
        )

