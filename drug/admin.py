# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from drug import models

# Register your models here.

admin.site.register(models.Drug)
admin.site.register(models.DrugCategory)
