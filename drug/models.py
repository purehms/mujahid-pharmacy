# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.
TEMP = (('Cool Area', 'Cool Area'),
        ('Dry Area', 'Dry Area'),
        ('Warm Area', 'Warm Area')
        )


class DrugCategory(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('sale:sale')


class Drug(models.Model):
    batch_no = models.CharField(max_length=100)
    drug_name = models.CharField(max_length=200)
    manufacture = models.CharField(max_length=200)
    category = models.ForeignKey(DrugCategory, on_delete=models.CASCADE)
    temperature = models.CharField(max_length=100, choices=TEMP)
    quantity = models.IntegerField(default=0)
    cost_price = models.IntegerField(default=0)
    production_date = models.DateField()
    expiry_date = models.DateField()
    total_cost = models.IntegerField()
    price_per_item = models.IntegerField()
    drug_location = models.CharField(max_length=500)
    visible = models.BooleanField(default=True)

    def __str__(self):
        return self.drug_name
