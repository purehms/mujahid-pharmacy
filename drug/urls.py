from django.conf.urls import url
from drug.views import (
    category,
    add_drug,
    view_drug,
    manage_drug,
    delete,
)

urlpatterns = [
    url(r'^$', category, name='category'),
    url(r'^(?P<key>[\d])/delete/$', delete, name='delete'),
    url(r'^add/$', add_drug, name='add_drug'),
    url(r'^view/$', view_drug, name='view_drug'),
    url(r'^manage/$', manage_drug, name='manage_drug'),
]
