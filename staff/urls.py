from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import logout
from views import (
    register,
    add_employee,
    registered_staff,
    view_staff,
    default,
    login_view,
    not_found,
    delete,
)
urlpatterns = [
    url(r'^$', login_view, name='login'),
    url(r'^default/$', default, name='default'),
    url(r'^log/$', logout, {'template_name': 'logout.html'}, name='logout'),
    url(r'^register/$', register, name='register'),
    url(r'^not-found/$', not_found, name='not_found'),
    url(r'^add-employee/$', add_employee, name='add_employee'),
    url(r'^registered-staffs/$', registered_staff, name='registered_staff'),
    url(r'^view-staff/$', view_staff, name='view_staff'),
    url(r'^(?P<key>[\d])/delete/$', delete, name='delete'),
]
