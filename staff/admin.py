# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from staff import models

# Register your models here.
admin.site.register(models.Staffs)
admin.site.register(models.Employee)
