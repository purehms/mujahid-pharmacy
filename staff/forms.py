from django import forms
from staff.models import Employee
from django.shortcuts import get_object_or_404
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
    )
User = get_user_model()

class LoginForm(forms.Form):
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'User Name'}), label='User-name:')
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password'}), label='Password:')


class RegisterForm(forms.Form):
    first_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'First Name'}), label='First Name:')
    last_name = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Last Name'}), label='Last Name:')
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Name'}), label='User Name:')
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password'}))
    is_admin = forms.BooleanField(required=False,)


class EmployeeForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = (
            'name', 'sex', 'statue', 'date_of_birth',
            'nationality', 'address', 'date_employed',
            'phone', 'email',
        )
