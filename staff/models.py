# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

STATUE = (('Married', 'Married'), ('Single', 'Single'))
SEX = (('Male', 'Male'), ('Female', 'Female'), ('Other', 'Other'))

# Create your models here.


class Staffs(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class Employee(models.Model):
    name = models.CharField(max_length=100)
# passport = models.ImageField(blank=True)
    sex = models.CharField(max_length=20, choices=SEX)
    statue = models.CharField(max_length=20, choices=STATUE)
    date_of_birth = models.DateField(max_length=10)
    nationality = models.CharField(max_length=100)
    address = models.TextField()
    phone = models.IntegerField()
    email = models.EmailField(blank=True)
    date_employed = models.DateField()
    visible = models.BooleanField(default=True)
