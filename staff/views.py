# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

from django.contrib import messages
from django.contrib.auth import (
    login,
    authenticate,
)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import (
    render,
    redirect,
    get_object_or_404,
)

from sale.models import ItemSale
from staff.forms import (
    RegisterForm,
    EmployeeForm,
    LoginForm,
)
from staff.models import (
    Staffs,
    Employee,
)
from drug.models import Drug
from utils import (
    get_user,
)


# Create your views here.


def login_view(request):
    next = request.GET.get('next')
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            if next:
                return redirect(next)
            return redirect('staff:default')
        messages.error(request, 'Username password does not match')
    return render(request, "base/registration/login.html", {"form": form})


def register(request):
    errors = {}
    next = request.GET.get('next')
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        user = User()
        is_admin = form.cleaned_data['is_admin']
        password = form.cleaned_data['password']
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.set_password(password)
        try:
            user.save()
            Staffs.objects.create(user=user, is_admin=is_admin)
            login(request, user)
            if next:
                return redirect(next)
            return redirect("/")
        except Exception as e:
            errors = {

                'message': e
            }

    context = {
        "form": form,
        'errors': errors,
    }
    return render(request, 'base/registration/register.html', context)


@login_required(login_url='/?next=/')
def default(request):
    today = date.today()
    a = 0
    drugs = Drug.objects.all()
    for drug in drugs:
        if drug.expiry_date <= today:
            a += 1

    context = {
        'expired': a,
        'staff': Staffs.objects.all(),
        'drugs': Drug.objects.filter(visible=True),
        'sale': ItemSale.objects.all(),
        'user': get_user(request.user),
    }
    return render(request, 'base/default_page.html', context)


@login_required(login_url='/?next=/')
def add_employee(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')

    form = EmployeeForm(request.POST or None)
    if form.is_valid():
        if form.is_valid():
            name = form.cleaned_data['name']
            sex = form.cleaned_data['sex']
            statue = form.cleaned_data['statue']
            date_of_birth = form.cleaned_data['date_of_birth']
            nationality = form.cleaned_data['nationality']
            address = form.cleaned_data['address']
            phone = form.cleaned_data['phone']
            email = form.cleaned_data['email']
            date_employed = form.cleaned_data['date_employed']
            Employee.objects.create(
                name=name, sex=sex,
                statue=statue, email=email,
                date_of_birth=date_of_birth,
                phone=phone, nationality=nationality,
                address=address,
                date_employed=date_employed
            )
            return redirect("/view-staff")
    context = {
        "form": form,
        'user': get_user(request.user),
    }
    return render(request, 'staff/employee.html', context)


@login_required(login_url='/?next=/')
def registered_staff(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    staff = Staffs.objects.all()
    context = {
        'staffs': staff,
        'user': user,
    }
    return render(request, 'staff/registered_staff.html', context)


@login_required(login_url='/?next=/')
def view_staff(request):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')

    context = {
        'employees': Employee.objects.all(),
        'user': get_user(request.user),
    }
    return render(request, 'staff/view_staff.html', context)


def not_found(request):
    context = {
        'employees': Employee.objects.all(),
        'user': get_user(request.user),
    }
    return render(request, '404.html', context)


@login_required(login_url='/?next=/')
def delete(request, key):
    user = get_user(request.user)
    if not user.is_admin:
        return redirect('/not-found')
    employee = get_object_or_404(Employee, id=key)
    employee.visible = False
    employee.save()
    context = {
        'employees': Employee.objects.all(),
        'user': get_user(request.user),
    }
    return render(request, 'staff/view_staff.html', context)
